# Overview
This document describes how I use [Packer](https://www.packer.io/) to
build a development environment suitable for [Vagrant](https://www.vagrantup.com/)/
[VirtualBox](https://www.virtualbox.org/).  The box is being used for both
personal and professional projects and is updated with each new VirtualBox
release.

# Prerequisites
* Working [Ubuntu](https://www.ubuntu.com/) installation
* Working [VirtualBox](https://www.virtualbox.org/) installation
* Working [Vagrant](https://www.vagrantup.com/) installation
* Working [Packer](https://www.packer.io/) installation
* Working [Atlas](https://atlas.hashicorp.com/) account

# Building
1. [Create Ubuntu Appliance](#create-ubuntu-appliance)
1. [Install Kubernetes](#install-kubernetes)
1. [Create The Bare Bones Vagrant Box](#create-the-bare-bones-vagrant-box)
1. [Install Required Ansible Roles](#install-required-ansible-roles)
1. [Create The Docker Vagrant Box](#create-the-docker-vagrant-box)
1. [Create The Development Vagrant Box](#create-the-development-vagrant-box)

Create Ubuntu Appliance
-----------------------
The first step in the process is to create an Ubuntu virtual machine
and export it as an appliance. This is done so you can make any configuration
tweaks that might be too difficult to do via automation, exporting your work
to the next step in the pipeline.

1. download the Ubuntu ISO
1. create the virtual machine
1. run the installer
1. export the machine as an appliance

Create The Bare Bones Vagrant Box
---------------------------------
The goal here is to get Packer to use the previously created appliance, install
VirtualBox's Guest Additions, configure passwordless sudo and create the Vagrant
box.  Luckily, this process is fully automated.

1. `git clone https://github.com/kurron/packer-server-xenial.git`
1. copy your virtual appliance to the directory you checked the code out to and
name it `Xenial-x64-Server.ova`.
1. edit `packer.json`, updating the `version` and `comment` variables as desired
1. `./build.sh` to create the new Vagrant box
1. commit and push your changes

Install Required Ansible Roles
------------------------------
The provisioning of the boxes is done using [Ansible](https://www.ansible.com/).
Ansible uses Roles, which are pre-packaged logic bundles that allow developers
to share their installation techniques. [Ansible Galaxy](https://galaxy.ansible.com/)
is the free resource that houses Roles.  All of mine are located
[here](https://galaxy.ansible.com/kurron/).  What we need to do is install the
required Roles into your box.

1. `git checkout docker` to switch to the docker branch
1. edit `packer.json`, updating the `version` and `comment` variables as desired
1. update the following Ansible Roles, ensuring current versions are being used
    * ansible-role-atlassian
    * ansible-role-aws
    * ansible-role-docker-host
    * ansible-role-hashicorp
    * ansible-role-javascript-developer
    * ansible-role-jdk
    * ansible-role-jvm-developer
    * ansible-role-jvpn
    * ansible-role-operations
    * ansible-role-python-developer
    * ansible-role-software-developer
    * ansible-role-sql-developer

1. *TODO:* update the Docker role so the current versions get installed
1. *TODO:* install the Ansible roles
1. commit and push your changes

Create The Docker Vagrant Box
-----------------------------
This step in the pipeline will use a Packer "hack" to take the previously
generated Vagrant box and install [Docker](https://www.docker.com/) into
it.  All we have to do is switch to a different branch and run another script.

1. export your Atlas information into your environment.  Examples follow.
1. `export VAGRANT_CLOUD_ACCOUNT=kurron`
1. `export VAGRANT_CLOUD_TOKEN=xOVR...`
1. edit `packer.json` so the `version` and `comment` fields are correct
1. `./build.sh` to create the new Vagrant box with Docker installed
1. `vagrant up` to start your new box
1. `vagrant ssh` to connect to your box so you can poke around
1. `docker info` while connected will prove you can interact with Docker
1. `vagrant destroy` to delete your test box

Create The Development Vagrant Box
----------------------------------
In this step we create Vagrant box running a Linux GUI and all the tools that
any self-respecting developer would want.  This box is based on the previously
built Docker box and adds additional software.

1. `git clone https://github.com/kurron/packer-xubuntu-xenial.git`
1. TODO: mention updating the Ansible Roles
1. Edit `packer.json` to reflect new comments and versions
1. `./build.sh` to build the new box
1. Edit `vagrantfile` and modify the minium Vagrant version

# Installation

# Tips and Tricks

# Troubleshooting

# License and Credits
This project is licensed under the [Apache License Version 2.0, January 2004](http://www.apache.org/licenses/).
